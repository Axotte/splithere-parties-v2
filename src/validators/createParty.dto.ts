import {
  ArrayMinSize,
  IsArray,
  IsDate,
  IsDefined,
  IsISO8601,
  IsNotEmpty,
  IsOptional,
  ValidateNested,
} from 'class-validator';
import { Address } from '../database/models/address.model';

export class CreatePartyDto {
  @IsNotEmpty()
  name: string;

  @IsOptional()
  description: string;

  @IsDefined()
  @IsISO8601()
  startDate: Date;

  @IsOptional()
  @ValidateNested()
  address: Address;

  @ArrayMinSize(1)
  @IsDefined()
  @IsArray()
  participantUuids: string[];
}
