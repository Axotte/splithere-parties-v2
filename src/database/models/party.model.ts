import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserDto } from '../../microservice/dtos/user.dto';
import { Address } from './address.model';
import { Bill } from './bill.model';
import { Charge } from './charge.model';
import { Participant } from './participant.model';

export enum PartyStatus {
  PUBLISHED = 'PUBLISHED',
  STARTED = 'STARTED',
  FINISHED = 'FINISHED',
}

@Entity()
export class Party {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column('simple-enum', { enum: PartyStatus, default: PartyStatus.PUBLISHED })
  status: PartyStatus;

  @Column({ nullable: true })
  description: string;

  @Column('date')
  startDate: Date;

  @Column(() => Address)
  address: Address;

  @ManyToOne(() => Participant)
  owner: Participant | UserDto;

  @ManyToMany(() => Participant)
  @JoinTable()
  participants: Participant[] | UserDto[];

  @OneToMany(() => Bill, (bill) => bill.party)
  bills: Bill[];

  @OneToMany(() => Charge, (charge) => charge.party)
  charges: Charge[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
