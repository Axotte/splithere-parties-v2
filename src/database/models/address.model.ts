import { Column } from 'typeorm';

export class Address {
  @Column({ nullable: true })
  city: string;

  @Column('decimal', { nullable: true })
  lat: number;

  @Column('decimal', { nullable: true })
  lon: number;

  @Column({ nullable: true })
  addressPart1: string;

  @Column({ nullable: true })
  addressPart2: string;
}
