import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientOptions, ClientProxyFactory } from '@nestjs/microservices';

@Module({
  providers: [
    {
      provide: 'AUTH_MICROSERVICE_CLIENT',
      useFactory: (configService: ConfigService) => {
        const authMicroserviceOptions = configService.get<ClientOptions>(
          'authMicroserviceOptions'
        );
        return ClientProxyFactory.create(authMicroserviceOptions);
      },
      inject: [ConfigService],
    },
    {
      provide: 'ACCOUNT_MICROSERVICE_CLIENT',
      useFactory: (configService: ConfigService) => {
        const accountMicroserviceOptions = configService.get<ClientOptions>(
          'accountMicroserviceOptions'
        );
        return ClientProxyFactory.create(accountMicroserviceOptions);
      },
      inject: [ConfigService],
    },
  ],
  imports: [ConfigModule],
  exports: ['AUTH_MICROSERVICE_CLIENT', 'ACCOUNT_MICROSERVICE_CLIENT'],
})
export class MicroserviceClientsModule {}
