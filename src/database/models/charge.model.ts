import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserDto } from '../../microservice/dtos/user.dto';
import { Participant } from './participant.model';
import { Party } from './party.model';

@Entity()
export class Charge {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('decimal')
  debt: number;

  @ManyToOne(() => Participant)
  debtor: Participant | UserDto;

  @ManyToOne(() => Participant)
  creditor: Participant | UserDto;

  @ManyToOne(() => Party, (party) => party.bills)
  party: Party;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
