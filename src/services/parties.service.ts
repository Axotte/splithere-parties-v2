import {
  BadRequestException,
  ConflictException,
  ForbiddenException,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { Connection } from 'typeorm';
import { Bill } from '../database/models/bill.model';
import { Charge } from '../database/models/charge.model';
import { Participant } from '../database/models/participant.model';
import { Party, PartyStatus } from '../database/models/party.model';
import { PartyRepository } from '../database/repositories/party.repository';
import { UserDto } from '../microservice/dtos/user.dto';
import { CreateBillDto } from '../validators/createBill.dto';
import { CreatePartyDto } from '../validators/createParty.dto';
import { UpdatePartyDto } from '../validators/updateParty.dto';
import { ProfilesService } from './profiles.service';

@Injectable()
export class PartiesService {
  constructor(
    private readonly profilesService: ProfilesService,
    private readonly partiesRepository: PartyRepository,
    @InjectConnection() private readonly connection: Connection
  ) {}

  getParties(currentUser: UserDto): Promise<Party[]> {
    return this.partiesRepository.getPartiesByUserUuid(currentUser.uuid);
  }

  getPartyDetails(id: number): Promise<Party> {
    return this.partiesRepository.getPartyWithDetails(id);
  }

  async createParty(
    currentUser: UserDto,
    newParty: CreatePartyDto
  ): Promise<void> {
    const users = await this.profilesService.getManyUsersByUuids(
      newParty.participantUuids
    );

    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const savedParticipants = await of(users)
        .pipe(
          map((users) =>
            users.map((user) => ({ participantUuid: user.uuid } as Participant))
          ),
          mergeMap((participants) =>
            Promise.all(
              participants.map((participant) =>
                queryRunner.manager.save(Participant, participant)
              )
            )
          )
        )
        .toPromise();

      const party: Party = {
        owner: { participantUuid: currentUser.uuid },
        name: newParty.name,
        startDate: newParty.startDate,
        description: newParty.description,
        participants: savedParticipants,
      } as Party;

      await queryRunner.manager.save(Participant, {
        participantUuid: currentUser.uuid,
      });
      await queryRunner.manager.save(Party, party);

      await queryRunner.commitTransaction();
      await queryRunner.release();
    } catch (e) {
      Logger.log(e);
      await queryRunner.rollbackTransaction();
      await queryRunner.release();

      throw new ConflictException('Failed to create party');
    }
  }

  async updateParty(
    id: number,
    currentUser: UserDto,
    partyToUpdate: UpdatePartyDto
  ): Promise<void> {
    const party = await this.partiesRepository.findOne(id, {
      relations: ['owner'],
    });

    if (!party) {
      throw new BadRequestException('Party with this id does not exist');
    } else if (
      (party.owner as Participant).participantUuid !== currentUser.uuid
    ) {
      throw new ForbiddenException('You are not ovner of this event');
    } else if (party.status !== PartyStatus.PUBLISHED) {
      throw new BadRequestException('You cannot edit this party');
    }

    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      if (partyToUpdate.participantUuids) {
        const newParticipants = await this.profilesService.getManyUsersByUuids(
          partyToUpdate.participantUuids
        );

        const savedParticipants = await of(newParticipants)
          .pipe(
            map((users) =>
              users.map(
                (user) => ({ participantUuid: user.uuid } as Participant)
              )
            ),
            mergeMap((participants) =>
              Promise.all(
                participants.map((participant) =>
                  queryRunner.manager.save(Participant, participant)
                )
              )
            )
          )
          .toPromise();

        party.participants = savedParticipants;

        delete partyToUpdate.participantUuids;
      }

      await queryRunner.manager.save(Party, { ...party, ...partyToUpdate });

      await queryRunner.commitTransaction();
      await queryRunner.release();
    } catch (e) {
      Logger.log(e);
      await queryRunner.rollbackTransaction();
      await queryRunner.release();

      throw new ConflictException('Failed to update party');
    }
  }

  async startParty(id: number, currentUser: UserDto): Promise<void> {
    const party = await this.partiesRepository.findOne(id, {
      relations: ['owner'],
    });

    if (!party) {
      throw new BadRequestException('Party with this id does not exist');
    } else if (
      (party.owner as Participant).participantUuid !== currentUser.uuid
    ) {
      throw new ForbiddenException('You are not ovner of this event');
    } else if (party.status !== PartyStatus.PUBLISHED) {
      throw new BadRequestException('You cannot start this party');
    }

    party.status = PartyStatus.STARTED;

    await this.partiesRepository.save(party);
  }

  async finishParty(id: number, currentUser: UserDto): Promise<void> {
    const party = await this.partiesRepository.findOne(id, {
      relations: ['owner'],
    });

    if (!party) {
      throw new BadRequestException('Party with this id does not exist');
    } else if (
      (party.owner as Participant).participantUuid !== currentUser.uuid
    ) {
      throw new ForbiddenException('You are not owner of this event');
    } else if (party.status !== PartyStatus.STARTED) {
      throw new BadRequestException('You cannot finish this party');
    }

    party.status = PartyStatus.FINISHED;

    await this.partiesRepository.save(party);
  }

  async getBills(id: number): Promise<Bill[]> {
    return (await this.partiesRepository.getPartyWithBills(id)).bills;
  }

  async getCharges(id: number): Promise<Charge[]> {
    return (await this.partiesRepository.getPartyWithCharges(id)).charges;
  }

  async addBill(user: UserDto, bill: CreateBillDto, id: number): Promise<void> {
    const amountPerUser = bill.amount / (bill.debtors.length + 1);
    const party = await this.partiesRepository.getPartyWithDetails(id);

    if (!party) {
      throw new NotFoundException();
    }

    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      for (const debtor of bill.debtors) {
        const charge = party.charges.find(
          (charge) =>
            ((charge.debtor as Participant).participantUuid === debtor &&
              (charge.creditor as Participant).participantUuid === user.uuid) ||
            ((charge.creditor as Participant).participantUuid === debtor &&
              (charge.debtor as Participant).participantUuid === user.uuid)
        );

        if (charge) {
          if ((charge.debtor as Participant).participantUuid === debtor) {
            charge.debt += amountPerUser;
          } else {
            charge.debt -= amountPerUser;

            if (charge.debt < 0) {
              charge.debt *= -1;
              const oldDebtor = charge.debtor;
              charge.debtor = charge.creditor;
              charge.creditor = oldDebtor;
            }
          }

          await queryRunner.manager.save(Charge, charge);
        } else {
          const savedDebtor = await queryRunner.manager.save(Participant, {
            participantUuid: debtor,
          });
          const savedCreditor = await queryRunner.manager.save(Participant, {
            participantUuid: user.uuid,
          });

          const newCharge: Charge = {
            debt: amountPerUser,
            debtor: savedDebtor,
            creditor: savedCreditor,
            party: party,
          } as Charge;

          const savedCharge = await queryRunner.manager.save(Charge, newCharge);
          party.charges.push(savedCharge);
        }

        const debtors = bill.debtors.map((i) => ({ participantUuid: i }));

        const billToSave = {
          payer: { participantUuid: user.uuid },
          debtors: debtors,
          amount: bill.amount,
          description: bill.description,
        } as Bill;

        const savedBill = await queryRunner.manager.save(Bill, billToSave);
        party.bills.push(savedBill);

        await queryRunner.manager.save(Party, party);
        await queryRunner.commitTransaction();
        await queryRunner.release();
      }
    } catch (e) {
      await queryRunner.rollbackTransaction();
      await queryRunner.release();

      throw new ConflictException('Failed to add bill');
    }
  }
}
