import { Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class Participant {
  @PrimaryColumn('uuid')
  participantUuid: string;
}
