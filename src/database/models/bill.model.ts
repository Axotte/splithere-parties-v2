import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserDto } from '../../microservice/dtos/user.dto';
import { Participant } from './participant.model';
import { Party } from './party.model';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  description: string;

  @Column('decimal')
  amount: number;

  @ManyToOne(() => Participant)
  payer: Participant;

  @ManyToMany(() => Participant)
  @JoinTable()
  debtors: Participant[] | UserDto[];

  @ManyToOne(() => Party, (party) => party.bills)
  party: Party;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
