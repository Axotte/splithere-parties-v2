import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
} from '@nestjs/common';
import { Bill } from '../../database/models/bill.model';
import { Charge } from '../../database/models/charge.model';
import { Party } from '../../database/models/party.model';
import { UserDto } from '../../microservice/dtos/user.dto';
import { PartiesService } from '../../services/parties.service';
import { CreateBillDto } from '../../validators/createBill.dto';
import { CreatePartyDto } from '../../validators/createParty.dto';
import { UpdatePartyDto } from '../../validators/updateParty.dto';
import { CurrentUser } from '../decorators/currentUser.decorator';

@Controller('parties')
export class PartiesController {
  constructor(private readonly partiesService: PartiesService) {}

  @Get()
  getParties(@CurrentUser() user: UserDto): Promise<Party[]> {
    return this.partiesService.getParties(user);
  }

  @Get(':id')
  getPartiesDetails(@Param('id', ParseIntPipe) id: number): Promise<Party> {
    return this.partiesService.getPartyDetails(id);
  }

  @Post()
  createParty(
    @CurrentUser() user: UserDto,
    @Body() party: CreatePartyDto
  ): Promise<void> {
    return this.partiesService.createParty(user, party);
  }

  @Patch(':id')
  updateParty(
    @CurrentUser() user: UserDto,
    @Body() party: UpdatePartyDto,
    @Param('id', ParseIntPipe) id: number
  ): Promise<void> {
    return this.partiesService.updateParty(id, user, party);
  }

  @Post(':id/start')
  startParty(
    @CurrentUser() user: UserDto,
    @Param('id', ParseIntPipe) id: number
  ): Promise<void> {
    return this.partiesService.startParty(id, user);
  }

  @Post(':id/finish')
  finishParty(
    @CurrentUser() user: UserDto,
    @Param('id', ParseIntPipe) id: number
  ): Promise<void> {
    return this.partiesService.finishParty(id, user);
  }

  @Get(':id/bills')
  getBills(@Param('id', ParseIntPipe) id: number): Promise<Bill[]> {
    return this.partiesService.getBills(id);
  }

  @Post(':id/bills')
  addBill(
    @CurrentUser() user: UserDto,
    @Body() bill: CreateBillDto,
    @Param('id', ParseIntPipe) id: number
  ): Promise<void> {
    return this.partiesService.addBill(user, bill, id);
  }

  @Get(':id/charges')
  getCharges(@Param('id', ParseIntPipe) id: number): Promise<Charge[]> {
    return this.partiesService.getCharges(id);
  }
}
