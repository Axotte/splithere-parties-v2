import {
  CanActivate,
  Injectable,
  ExecutionContext,
  Inject,
  RequestTimeoutException,
  UnauthorizedException,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { timeout, catchError } from 'rxjs/operators';
import { TimeoutError, throwError } from 'rxjs';
import { UserDto } from '../../microservice/dtos/user.dto';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    @Inject('AUTH_MICROSERVICE_CLIENT') private readonly authClient: ClientProxy
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const token = req.get('authorization');

    if (!token) {
      throw new UnauthorizedException('Missing authorization header');
    }

    const user = await this.authClient
      .send<UserDto>(
        {
          cmd: 'authenticate',
        },
        token
      )
      .pipe(
        timeout(5000),
        catchError((e) => {
          if (e instanceof TimeoutError) {
            return throwError(new RequestTimeoutException());
          } else {
            return throwError(new UnauthorizedException(e.message));
          }
        })
      )
      .toPromise();

    req.user = user;

    return true;
  }
}
