import {
  ArrayMinSize,
  IsArray,
  IsDefined,
  IsNotEmpty,
  IsPositive,
} from 'class-validator';

export class CreateBillDto {
  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  @IsPositive()
  amount: number;

  @IsDefined()
  @ArrayMinSize(1)
  @IsArray()
  debtors: string[];
}
