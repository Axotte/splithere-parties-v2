import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PartyRepository } from '../database/repositories/party.repository';
import { MicroserviceClientsModule } from '../microservice/microserviceClients.module';
import { PartiesService } from '../services/parties.service';
import { ProfilesService } from '../services/profiles.service';
import { PartiesController } from './controllers/parties.controller';

@Module({
  providers: [PartiesService, ProfilesService],
  controllers: [PartiesController],
  imports: [
    MicroserviceClientsModule,
    TypeOrmModule.forFeature([PartyRepository]),
  ],
})
export class PartiesModule {}
