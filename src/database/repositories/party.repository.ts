import { EntityRepository, Repository } from 'typeorm';
import { Party } from '../models/party.model';

@EntityRepository(Party)
export class PartyRepository extends Repository<Party> {
  getPartiesByUserUuid(uuid: string): Promise<Party[]> {
    return this.createQueryBuilder('party')
      .leftJoin('party.participants', 'participants')
      .innerJoin('party.owner', 'owner')
      .where('participants.participantUuid = :uuid', { uuid })
      .orWhere('owner.participantUuid = :uuid', { uuid })
      .getMany();
  }

  getPartyById(id: number): Promise<Party> {
    return this.createQueryBuilder('party')
      .leftJoinAndSelect('party.owner', 'owner')
      .leftJoinAndSelect('party.participants', 'participants')
      .where('party.id = :id', { id })
      .getOne();
  }

  getPartyWithDetails(id: number): Promise<Party> {
    return this.createQueryBuilder('party')
      .leftJoinAndSelect('party.participants', 'participants')
      .innerJoinAndSelect('party.owner', 'owner')
      .leftJoinAndSelect('party.bills', 'bills')
      .leftJoinAndSelect('party.charges', 'charges')
      .leftJoinAndSelect('bills.debtors', 'debtors')
      .leftJoinAndSelect('bills.payer', 'payer')
      .leftJoinAndSelect('charges.debtor', 'debtor')
      .leftJoinAndSelect('charges.creditor', 'creditor')
      .where('party.id = :id', { id })
      .getOne();
  }

  getPartyWithBills(id: number): Promise<Party> {
    return this.createQueryBuilder('party')
      .leftJoinAndSelect('party.bills', 'bills')
      .leftJoinAndSelect('bills.debtors', 'debtors')
      .leftJoinAndSelect('bills.payer', 'payer')
      .where('party.id = :id', { id })
      .getOne();
  }

  getPartyWithCharges(id: number): Promise<Party> {
    return this.createQueryBuilder('party')
      .leftJoinAndSelect('party.charges', 'charges')
      .leftJoinAndSelect('charges.debtor', 'debtor')
      .leftJoinAndSelect('charges.creditor', 'creditor')
      .where('party.id = :id', { id })
      .getOne();
  }
}
