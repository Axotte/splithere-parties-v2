import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const dbConfig: Record<string, TypeOrmModuleOptions> = {
  develop: {
    name: 'splithere-db',
    type: 'sqlite',
    database: ':memory:',
    synchronize: true,
    logging: true,
  },
  test: {
    type: 'postgres',
    host: 'db',
    port: 5432,
    username: 'postgres',
    password: 'Test123',
    database: 'parties_db',
    synchronize: true,
    logging: false,
    extra: {
      connectionLimit: 5,
    },
  },
};
