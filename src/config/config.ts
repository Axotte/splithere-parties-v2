/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { dbConfig } from './dbConfig';
import { Transport } from '@nestjs/microservices';

export default () => ({
  db: dbConfig[process.env.ENV || 'develop'],
  accountMicroserviceOptions: {
    transport: Transport.RMQ,
    options: {
      urls: [process.env.RABBITMQ_URL || 'amqp://localhost:5672'],
      queue: 'account_queue',
    },
  },
  authMicroserviceOptions: {
    transport: Transport.RMQ,
    options: {
      urls: [process.env.RABBITMQ_URL || 'amqp://localhost:5672'],
      queue: 'auth_queue',
    },
  },
});
