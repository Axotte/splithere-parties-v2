import { Module } from '@nestjs/common';
import configuration from './config/config';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { AuthGuard } from './api/guards/auth.guard';
import { MicroserviceClientsModule } from './microservice/microserviceClients.module';
import { Bill } from './database/models/bill.model';
import { Charge } from './database/models/charge.model';
import { Participant } from './database/models/participant.model';
import { Party } from './database/models/party.model';
import { PartiesModule } from './api/parties.module';

@Module({
  imports: [
    MicroserviceClientsModule,
    PartiesModule,
    ConfigModule.forRoot({ load: [configuration] }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          ...configService.get<TypeOrmModuleOptions>('db'),
          entities: [Bill, Charge, Participant, Party],
        };
      },
    }),
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule {}
