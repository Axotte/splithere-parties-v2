import {
  BadRequestException,
  Inject,
  Injectable,
  RequestTimeoutException,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { catchError, tap, timeout } from 'rxjs/operators';
import { UserDto } from '../microservice/dtos/user.dto';

@Injectable()
export class ProfilesService {
  constructor(
    @Inject('ACCOUNT_MICROSERVICE_CLIENT')
    private readonly accountClient: ClientProxy
  ) {}

  getUserByUuid(uuid: string): Promise<UserDto> {
    return this.accountClient
      .send({ cmd: 'getUser' }, uuid)
      .pipe(
        timeout(5000),
        catchError(() => {
          throw new RequestTimeoutException();
        }),
        tap((user) => {
          if (!user) throw new BadRequestException('Invalid user uuid');
        })
      )
      .toPromise();
  }

  getManyUsersByUuids(uuids: string[]): Promise<UserDto[]> {
    return this.accountClient
      .send({ cmd: 'getMultipleUsersByUuids' }, uuids)
      .pipe(
        timeout(5000),
        catchError(() => {
          throw new RequestTimeoutException();
        }),
        tap<UserDto[]>((users) => {
          if ((users?.length ?? 0) !== uuids.length) {
            throw new BadRequestException('Invalid user uuid');
          }
        })
      )
      .toPromise();
  }
}
