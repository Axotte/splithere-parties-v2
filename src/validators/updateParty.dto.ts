import {
  IsArray,
  IsDate,
  IsNotEmpty,
  IsOptional,
  ValidateNested,
} from 'class-validator';
import { Address } from '../database/models/address.model';

export class UpdatePartyDto {
  @IsOptional()
  @IsNotEmpty()
  name: string;

  @IsOptional()
  description: string;

  @IsOptional()
  @IsDate()
  startDate: Date;

  @IsOptional()
  @ValidateNested()
  address: Address;

  @IsOptional()
  @IsArray()
  participantUuids: string[];
}
